<?php
/**
 * File Comment
 *
 * @category Unknown
 * @package  Kodutoo
 * @author   Siim Bobkov <sibobk@ttu.ee>
 * @license  MIT neti.ee
 * @link     http://url.com
 */


require_once "lib/tpl.php";
require_once 'contact.php';
require_once 'contactList.php';

$cmd = isset($_GET["cmd"]) ? $_GET["cmd"] : "list_page";

function printJson($value) {
    header("Content-type: application/json");
    echo json_encode($value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
}

if ($cmd === 'list_page' && $_SERVER["REQUEST_METHOD"] === "GET") {
    $contacts = getAllContacts();
    $data = ['$contacts' => $contacts];
    print renderTemplate('views/main.html', $data);
}elseif ($cmd === 'edit_page') {
    $id= $_GET['person_id'];
    $contact=getContact($id);
    $data = ['$contact' => $contact];
    print renderTemplate('views/main.html', $data);
}elseif ($cmd === 'add_page') {
    $data = ['$contacts' => null];
    print renderTemplate('views/main.html');
} elseif ($cmd === 'add_contact' && $_SERVER["REQUEST_METHOD"] == "POST") {
    $input = json_encode($_POST);
    $input = json_decode($input);
    $phones[] = $input->phone1;
    $phones[] = $input->phone2;
    $phones[] = $input->phone3;
    $contact = new Contact($input->firstName, $input->lastName, $phones);
    $contact->id=$input->contactId;
    $errors = $contact->validate();
    if (count($errors) > 0) {
        //saadame veateate
        //        http_response_code(400);
        //        header("Content-type: application/json");
        $response = ["errors" => $errors];
        //        echo json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        $data = ['$contact' => $contact, '$errors' =>$errors];
        print renderTemplate('views/main.html', $data);
    } else {
        addContact($contact);
        $contacts = getAllContacts();
        $data = ['$contacts' => $contacts, '$errors' =>$errors];
        print renderTemplate('views/main.html', $data);
    }
}
