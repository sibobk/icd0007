<?php

/**
 * File Comment
 *
 * @category Unknown
 * @package  Kodutoo
 * @author   Siim Bobkov <sibobk@ttu.ee>
 * @license  MIT neti.ee
 * @link     http://url.com
 */

/**
 * Here is source file
 */
class Database
{
    private $username = 'sibobk';
    private $password = 'edbb';
    private $address = 'mysql:host=db.mkalmo.xyz;dbname=sibobk';
    public $connection;

    public function __construct() {
        $this->connection = new PDO(
            $this->address,
            $this->username,
            $this->password,
            [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
        );
    }

}

const DATA_FILE = "data/contacts.txt";

function getAllContacts() {
    $db = new Database();
    $stmt = $db->connection->prepare(
        'SELECT * FROM contacts, phones WHERE contacts.id=phones.contact_id'
    );
    $stmt->execute();
    $listContacts = [];
    foreach ($stmt as $row) {
        if (isset($listContacts[$row['id']])) {
            $listContacts[$row['id']]->addPhoneNumber($row["number"]);
        } else {
            $contact = new Contact($row['firstName'], $row['lastName'], $row['number']);
            $contact->id = $row['id'];
            $listContacts[$contact->id] = $contact;
        }
    }
    return $listContacts;
}

function getContact($id) {
    $db = new Database();
    $stmt = $db->connection->prepare(
        'SELECT * FROM contacts, phones WHERE contacts.id=phones.contact_id AND contacts.id=:id'
    );

    $stmt->execute(array('id' => $id));
    $listContacts = [];
    foreach ($stmt as $row) {
        if (isset($listContacts[$row['id']])) {
            $listContacts[$row['id']]->addPhoneNumber($row["number"]);
        } else {
            $contact = new Contact($row['firstName'], $row['lastName'], $row['number']);
            $contact->id = $row['id'];
            $listContacts[$contact->id] = $contact;
        }
    }
    return $listContacts[$id];
}


function addContact($contact) {
    $db = new Database();
    $id=$contact->id;
    if (($contact->id) ==="") {

        $stmt = $db->connection->prepare(
            'INSERT INTO contacts (firstName, lastName) values (:firstName, :lastName)'
        );
        $stmt->execute(array(
            'firstName' => $contact->firstName,
            'lastName' => $contact->lastName
        ));
        $id = $db->connection->lastInsertId();
    } else{
        $stmt = $db->connection->prepare(
            'UPDATE contacts SET firstName = :firstName, lastName = :lastName
                       WHERE id = :contact_id'
        );
        $stmt->execute(array(
                'firstName' => $contact->firstName,
                'lastName' => $contact->lastName,
                'contact_id' => $contact->id,
            ));
        $stmt = $db->connection->prepare(
                'DELETE FROM phones WHERE contact_id= :contact_id'
            );
        $stmt->execute(array(
                'contact_id' => $contact->id
            ));
        $id=$contact->id;
    }
    $stmt = $db->connection->prepare(
        'INSERT INTO phones (contact_id, number) VALUES (:contact_id, :phoneNumber)'
    );
    foreach ($contact->phones as $phoneNumber) {
        if ($phoneNumber !== '') {
            $stmt->execute(array(
                'contact_id' => $id,
                'phoneNumber' => $phoneNumber
            ));
        }
    }
}


function deleteContact($contact) {
    $contacts = getAllContacts();
    $index = array_search($contact, $contacts);
    if ($index !== false) {
        array_splice($contacts, $index, 1);
        file_put_contents(DATA_FILE, implode(PHP_EOL, $contacts));
        file_put_contents(DATA_FILE, PHP_EOL, FILE_APPEND);
    } else {
        print '<$contact> does not exist in the Contacts list!';
    }
}

function deleteAllContacts() {
    file_put_contents(DATA_FILE, '');
}


