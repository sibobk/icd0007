<?php

/**
 * File Comment
 *
 * @category Unknown
 * @package  Kodutoo
 * @author   Siim Bobkov <sibobk@ttu.ee>
 * @license  MIT neti.ee
 * @link     http://url.com
 */


/**
 * Class Comment
 *
 * @category Contact
 * @package  Kodutöö
 * @author   Siim Bobkov <sibobk@ttu.ee>
 * @license  MIT neti.ee
 * @link     http://url.com
 */
class Contact
{
    public $id;
    public $firstName;
    public $lastName;
    public $phones = [];


    /**
     * Contact constructor.
     *
     * @param $firstName users first name
     * @param $lastName  users last name
     * @param $phoneNumber     users phone number
     */
    public function __construct($firstName, $lastName, $phoneNumber) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        //        $this->phones[] = $phones;
        $this->addPhoneNumber($phoneNumber);
    }

    public function getPhoneNumbers() {
        return implode(", ", $this->phones);
    }

    public function addPhoneNumber($phoneNumber) {
        if (is_array($phoneNumber)) {
            $this->phones = [];
            foreach ($phoneNumber as $value) {
                if ($value != "") {
                    $this->phones[] = $value;
                }
            }
        } else {
            if ($phoneNumber != "") {
                $this->phones[] = $phoneNumber;
            }
        }
    }

    public function validate() {
        $errors = [];

        if (strlen($this->firstName) < 2) {
            $errors[] = "First name should have at least 2 characters";
        }
        if (strlen($this->lastName) < 2) {
            $errors[] = "Last name should have at least 2 characters";
        }
        if (count($this->phones) < 1) {
            $errors[] = "At least 1 phone number must be set";
        }

        return $errors;
    }

}
